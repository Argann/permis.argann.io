var questionSpan = document.getElementById("question");
var reponseSpan = document.getElementById("reponse");
var suivant = document.getElementById("suivant");
var badge = document.getElementById("badge");

var showResponse = true;
var derniereQuestionPremierSecours = true;

var currentQuestion = {};

var index = 0;

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

shuffle(questionsGenerales);
shuffle(questionsPremiersSecours);

function onClickSuivant()
{
    showResponse = !showResponse;

    if (showResponse)
    {
        reponseSpan.innerHTML = currentQuestion["reponse"];
        suivant.innerText = "Suivant !";
    }
    else
    {
        if (derniereQuestionPremierSecours)
        {
            currentQuestion = questionsGenerales[index % questionsGenerales.length];
            badge.innerText = "Question Générale ("+(index+1) + "/" + questionsGenerales.length + ")";
            badge.classList.add("badge-primary");
            badge.classList.remove("badge-danger");
        }
        else
        {
            currentQuestion = questionsPremiersSecours[index % questionsPremiersSecours.length];
            badge.innerText = "Premiers Secours ("+(index+1) + "/" + questionsPremiersSecours.length + ")";
            badge.classList.remove("badge-primary");
            badge.classList.add("badge-danger");

            index++;
        }

        derniereQuestionPremierSecours = !derniereQuestionPremierSecours;
        reponseSpan.innerHTML = "";
        questionSpan.innerHTML = currentQuestion["question"];
        suivant.innerText = "Réponse ?";
    }
}

suivant.onclick = onClickSuivant;

onClickSuivant();