var questionsGenerales = [
    {
        question: "Quelle est l'utilité de l'appuie-tête ?",
        reponse: "Permet de retenir le mouvement de la tête en cas de choc et limiter les blessures."
    },
    {
        question: "Pourquoi est-il important de bien régler son volant ? (Deux exemples)",
        reponse: "Pour un confort de conduite, <br/>pour l'accessibilité aux commandes, <br/>pour la visibilité du tableau de bord, <br/>pour l'efficacité des airbags."
    },
    {
        question: "Quel est l'intérêt du mode 'nuit' sur le rétroviseur ?",
        reponse: "Ne pas être ébloui par les feux du véhicule suiveur."
    },
    {
        question: "Peut-on fixer tous types de siège enfant sur des attaches de type Isofix?",
        reponse: "Non, uniquement les sièges compatibles."
    },
    {
        question: "Quelles sont les précautions à remplir lors du remplissage du réservoir ?",
        reponse: "Arrêter le moteur <br/>Ne pas fumer <br/>Ne pas téléphoner"
    },
    {
        question: "Quelles précautions dois-je prendre pour que les enfants installés à l'arrière ne puissent pas ouvrir leur portière ?",
        reponse: "Actionner la sécurité enfant sur les deux portières arrière."
    },
    {
        question: "Qu'est-ce qui peut provoquer la décharge de la batterie, moteur éteint ?",
        reponse: "Les feux ou accessoires électriques en fonctionnement."
    },
    {
        question: "En règle générale, à partir de quel âge un enfant peut-il être installé sur le siège passager avant du véhicule ?",
        reponse: "10 ans."
    },
    {
        question: "A quelle fréquence est-il préconisé de vérifier la pression d'air des pneumatiques ?",
        reponse: "Tous les mois."
    },
    {
        question: "Quelles sont les conditions à respécter pour contrôler le niveau d'huile ?",
        reponse: "Moteur froid et sur un terrain plat."
    },
    {
        question: "Quelle est la conséquence d'une température trop élevée de l'huile moteur ?",
        reponse: "Une surchauffe ou une casse du moteur."
    },
    {
        question: "Quelle est la différence entre un voyant orange et un voyant rouge sur le tableau de bord ?",
        reponse: "<strong>Rouge</strong> : Anomalie de fonctionnement ou danger<br/><strong>Orange</strong> : élément important"
    },
    {
        question: "Quel est le risque de circuler avec un frein de parking mal desserré ?",
        reponse: "Une dégradation du système de freinage."
    },
    {
        question: "Quel est le risque de maintenir les deux de route lors d'un croisement avec d'autres usagers ?",
        reponse: "Un risque d'éblouissement des autres usagers."
    },
    {
        question: "Pouvez-vous utiliser les feux de brouillard arrière par forte pluie ?",
        reponse: "Non."
    },
    {
        question: "Comment détecter l'usure des essuie-glaces en circulation ?",
        reponse: "En cas de pluie, ils laissent des tracent sur le pare-brise."
    },
    {
        question: "Pour une bonne visibilité vers l'arrière, en plus de l'utilisation de l'essuie-glace, quelle commande pouvez-vous actionner par temps de pluie ?",
        reponse: "La commande de désembuage arrière (dégivrage)."
    },
    {
        question: "Quelle est l'utilité d'un limiteur de vitesse ?",
        reponse: "Ne pas dépasser la vitesse programmée par le conducteur."
    },
    {
        question: "Sans actionner la commande du régulateur, comment le désactiver rapidement ?",
        reponse: "En appuyant sur la pédale de frein ou d'embrayage."
    },
    {
        question: "Pourquoi doit-on régler la hauteur des feux ?",
        reponse: "Pour ne pas éblouir les autres usagers."
    },
    {
        question: "Citez deux éléments complémentaires permettant un désembuage efficace.",
        reponse: "- La commande de vitesse de ventilation<br/>- La commande d'air chaud<br/>- La climatisation"
    },
    {
        question: "Quel est le risque de maintenir le recyclage de l'air de manière prolongée ?",
        reponse: "Un risque de mauvaise visibilité par l'apparition de buées sur les surfaces vitrées."
    },
    {
        question: "Dans quels cas doit-on utiliser les feux de détresse ?",
        reponse: "En cas de panne, d'accident ou de ralentissement important."
    },
    {
        question: "Dans quelle situation doit-on désactiver l'airbag du passager avant ?",
        reponse: "Lors du transport d'un enfant à l'avant dans un siège auto dos à la route."
    },
    {
        question: "Quelle peut être la conséquence d'une panne de dégivrage de la lunette arrière ?",
        reponse: "Une insiffisance ou une absence de visibilité vers l'arrière."
    },
    {
        question: "Dans quel cas peut-on utiliser l'avertisseur sonore en agglomération ?",
        reponse: "En cas de danger immédiat."
    },
    {
        question: "A partir de quel taux d'alcool, en période de permis probatoire, est-on en infraction ?",
        reponse: "0,20g/l de sang, soit zéro verre."
    },
    {
        question: "Quels sont les documents obligatoires à présenter lors d'un contrôle de police ?",
        reponse: "- L'attestation d'assurance<br/>- La vignette d'assurange<br/>- Le certificat d'immatriculation<br/>- Le permis de conduire"
    },
    {
        question: "En cas de panne ou d'accident, quels sont les accessoires de sécurité obligatoires ?",
        reponse: "- Le gilet de haute visibilité<br/>- Le triangle de présignalisation"
    },
    {
        question: "En cas d'accident, dans quel délai doit être transmis le constat amiable à l'assureur ?",
        reponse: "5 jours."
    },
    {
        question: "Par temps clair, à quelle distance les feux de positions doivent-ils être visibles ?",
        reponse: "150 mètres."
    },
    {
        question: "Quel est l'utilité des feux diurnes ?",
        reponse: "Rendre plus visible le véhicule de jour."
    },
    {
        question: "Quelles sont les conséquences d'un mauvais réglage des feux de croisement ?",
        reponse: "- Une mauvaise vision à l'avant<br/>- Un risque d'éblouissement des autres usagers."
    },
    {
        question: "Quelles sont les conséquences en cas de panne d'un feu de croisement ?",
        reponse: "Mauvaise visibilité et risque d'être confondu avec un deux-roues."
    },
    {
        question: "Quelle est l'utilité des dispositifs réfléchissants ?",
        reponse: "Rendre visible le véhicule la nuit."
    },
    {
        question: "Quelle est la signification d'un clignotement plus rapide des clignotants ?",
        reponse: "Non-fonctionnement d'une des ampoules."
    },
    {
        question: "Quelle est la conséquence en cas de panne des feux de stop ?",
        reponse: "Un manque d'information pour les usagers suiveurs et un risque de collision."
    },
    {
        question: "Quelles sont les deux utilités des feux de reculs ?",
        reponse: "- Eclairer la zone de recul la nuit <br/>- Avertir les autres usagers de la manoeuvre."
    },
    {
        question: "Dans quels cas utilise-t-on les feux de brouillard arrière ?",
        reponse: "- Par temps de brouillard<br/>- Par temps de neige"
    },
    {
        question: "Quelles sont les conséquences en cas de panne d'un feu de position arrière ?",
        reponse: "- Être mal vu<br/>- Un risque de collision"
    },
    {
        question: "Citez un endroit où l'on peut trouver les pressions préconisées pour les pneumatiques.",
        reponse: "- Une plaque sur la portière<br/>- Dans la notice d'utilisation du véhicule<br/>- Au niveau de la trappe à carburant"
    },
    {
        question: "Qu'est-ce que l'aquaplanage, et quelle peut être sa conséquence ?",
        reponse: "La présence d'un film d'eau entre le pneumatique et la chaussée pouvant entraîner une perte de contrôle du véhicule."
    },
    {
        question: "A quelle fréquence est-il préconisé de vérifier la pression des pneus ?",
        reponse: "Chaque mois, et avant chaque long trajet."
    },
    {
        question: "Pour un capot s'ouvrant depuis l'avant du véhicule, quelle est l'utilité du dispositif de sécurité ?",
        reponse: "Empêcher l'ouverture du capot en circulation en cas de mauvais verrouillage."
    },
    {
        question: "En roulant, quel est le risque d'une mauvaise fermeture du capot ?",
        reponse: "Un risque d'ouverture du capot pouvant entraîner un accident."
    },
    {
        question: "Pourquoi est-il préférable d'utiliser un produit lave-glace spécial en hiver ?",
        reponse: "Pour éviter le gel du liquide ?"
    },
    {
        question: "Quel est le risque principal d'une absence de liquide lave-glace ?",
        reponse: "Une mauvaise visibilité"
    },
    {
        question: "Quelle est la solution en cas de panne de batterie pour démarrer le véhicule sans le déplacer ?",
        reponse: "Brancher une deuxième batterie en parallèle (les '+' ensemble et les '-' ensemble) ou la remplacer."
    },
    {
        question: "Quel est le danger si l'on complète le niveau du liquide de refroidissement lorsque le moteur est chaud ?",
        reponse: "Un risque de brûlure."
    },
    {
        question: "Quelle est la conséquence d'un niveau insuffisant du liquide de frein ?",
        reponse: "Une perte d'efficacité du freinage"
    },
    {
        question: "Quel est le risque d'un manque d'huile moteur ?",
        reponse: "Un risque de détérioration ou de casse du moteur"
    },
    {
        question: "Quelles sont les précautions à prendre en cas d'installation d'un porte-vélo ?",
        reponse: "La plaque d'immatriculation et les feux doivent être visibles."
    },
    {
        question: "Un défaut d'éclairage de la plaque d'immatriculation lors du contrôle technique entraîne-t-il une contre-visite ?",
        reponse: "Oui."
    },
    {
        question: "Utilise-t-on le triangle de présignalisation sur autoroute ?",
        reponse: "Non."
    },
    {
        question: "Quels sont les risques de circuler avec des objets sur la plage arrière ?",
        reponse: "- Une mauvaise visibilité vers l'arrière<br/>- Un risque de projection en cas de freinage brusque ou de choc"
    },
    {
        question: "Lorsque vous transportez un poids important dans le coffre, quelles sont les précautions à prendre en ce qui concerne les pneumatiques et l'éclairage avant ?",
        reponse: "- Augmenter la pression des pneus<br/>- Baisser les feux avant"
    },
    {
        question: "Quel est le risque de circuler avec des balais d'essuie-glaces défectueux ?",
        reponse: "Avoir une mauvaise visibilité en cas d'intempéries."
    },
    {
        question: "Quelle est la principale conséquence d'un dispositif de lave-glace défaillant ?",
        reponse: "Mauvaise visibilité dûe à l'impossibilité de nettoyer le pare-brise."
    },
    {
        question: "Si la sécurité enfant est enclenchée, est-il possible d'ouvrir la portière arrière depuis l'extérieur ?",
        reponse: "Oui."
    }
];

var questionsPremiersSecours = [
    {
        question: "Comment et pourquoi protéger une zone de danger en cas d'accident de la route ?",
        reponse: "En délimitant clairement et largement la zone de danger de façon visible pour protéger les victimes et éviter un sur-accident."
    },
    {
        question: "En cas de panne ou d'accident, quel équipement de sécurité doit être porté avant de quitter le véhicule ?",
        reponse: "Le gilet de haute visibilité."
    },
    {
        question: "Hors autoroute ou endroit dangereux, en cas de panne ou d'accident, où doit être placé le triangle de présignalisation ?",
        reponse: "A 30m de la panne/accident, ou avant un virage ou un sommet de côte."
    },
    {
        question: "Dans quelle situation peut-on déplacer une victime ?",
        reponse: "En cas de présence d'un danger réel, immédiat, non contrôlable. Cas exceptionnel uniquement."
    },
    {
        question: "Quelles sont les conditions pour réaliser le dégagement d'urgence d'une victime en présence d'un danger réel, immédiat et non contrôlable ?",
        reponse: "- Victime visible<br/>- Victime facile à atteindre<br/>- Rien ne doit gêner le dégagement<br/>Il <strong>faut</strong> être sûr de pouvoir réaliser le dégagement de la victime."
    },
    {
        question: "Si un dégagement d'urgence de la victime est nécessaire, où doit-elle être déplacée ?",
        reponse: "Dans un endroit éloigné du danger et de ses conséquences."
    },
    {
        question: "Sur autoroute, comment indiquer avec précision les lieux de l'accident depuis un téléphone portable ?",
        reponse: "En indiquant :<br/>- Le numéro de l'autoroute<br/>- Le sens de circulation<br/>- Le point kilométrique"
    },
    {
        question: "Quels sont les numéros d'urgence à composer ?",
        reponse: "- Le 18 (Pompiers)<br/>- Le 15 (SAMU)<br/>- Le 112 (Urgence en UE)"
    },
    {
        question: "Par quels moyens doit être réalisée l'alerte des secours ?",
        reponse: "- A l'aide d'un téléphone portable<br/>- Ou un téléphone fixe<br/>- Ou une borne d'appel d'urgence"
    },
    {
        question: "Quelles sont les trois informations à transmettre aux services de secours ?",
        reponse: "- Le numéro de téléphone appelant<br/>- La nature du problème<br/>- La localisation la plus précise"
    },
    {
        question: "Pourquoi l'alerte auprès des services de secours doit être rapide et précise ?",
        reponse: "Pour permettre aux services de secours d'apporter <strong>les moyens adaptés</strong> aux victimes dans <strong>le délai le plus court</strong>"
    },
    {
        question: "A quel moment pouvez-vous mettre fin à un appel avec les secours ?",
        reponse: "Uniquement lorsque le correspondant nous invite à le faire."
    },
    {
        question: "Lors d'un appel avec les secours, pourquoi devez-vous attendre que votre correspondant vous autorise à raccrocher ?",
        reponse: "Car il peut nous conseiller ou nous guider dans la réalisation des gestes à faire, ou ne pas faire, jusqu'à l'arrivée des secours."
    },
    {
        question: "Quel est l'objectif du Signal d'Alerte et d'Informations des Populations (SAIP) ?",
        reponse: "Avertir la population d'un danger imminent ou qu'un évènement grave est en train de se produire."
    },
    {
        question: "Comment est composé le signal d'alerte du Système d'Alerte et d'Information des Populations (SAIP) diffusé par les sirènes ?",
        reponse: "Deux signaux :<br/>- le Signal National d'Alerte (SNA) sur trois cycles successifs<br/>- Le signal de fin d'alerte qui est continu"
    },
    {
        question: "Comment est diffusée l'alerte émise par le Système d'Alerte et d'Information des Populations (SAIP) ?",
        reponse: "Grâce :<br/>- Aux sirènes<br/>- Aux médias (Radio France, France Télévision, etc.)"
    },
    {
        question: "Quel comportement adopter en cas de diffusion du signal d'alerte du Système d'Alerte et d'Information des Populations (SAIP) ?",
        reponse: "- Se mettre en sécurité<br/>-S'informer grâce aux médias et sites internets des autorités dès que leur consultation est possible<br/>- Respecter les consignes des autorités"
    },
    {
        question: "Qu'est ce qu'une perte de connaissance ?",
        reponse: "C'est lorsque la victime :<br/>- Ne répond pas<br/>- Ne réagit pas<br/>- Mais respire"
    },
    {
        question: "Comment vérifier la respiration d'une victime ?",
        reponse: "- Est-ce que le ventre et la poitrine se soulèvent ?<br/>- Sentir de l'air à l'expiration"
    },
    {
        question: "Citez les trois manières d'évaluer l'état de conscience d'une victime",
        reponse: "- Lui poser des questions simples (comment ça va ? Vous m'entendez ?)<br/>- Lui secouer doucement les épaules<br/>- Lui prendre la main en lui demandant d'exécuter un geste simple 'serrez-moi la main'."
    },
    {
        question: "Quels sont les risques pour une personne en perte de connaissance qui est allongée sur le dos ?",
        reponse: "L'arrêt respiratoire et l'arrêt cardiaque"
    },
    {
        question: "Dans quel cas peut-on positionner une victime en position latérale de sécurité ?",
        reponse: "Si la victime ne répond pas, ne réagit pas, mais respire."
    },
    {
        question: "Quels comportements adopter en présence d'une victime qui ne répond pas, ne réagit pas, mais respire ?",
        reponse: "- La placer en PLS<br/>- Alerter les secours<br/>- Surveiller la respiration de la victime jusqu'à l'arrivée des secours"
    },
    {
        question: "Pourquoi ne faut-il pas laisser une personne en perte de connaissance allongée sur le dos ?",
        reponse: "Car elle risque un érouffement par :<br/>- Des liquides présents dans la gorge<br/>- la chute de la langue en arrière"
    },
    {
        question: "Pourquoi faut-il pratiquer immédiatement une réanimation cardio-pulmonaire sur une victime en arrêt cardiaque ?",
        reponse: "Car les lésions du cerveau (consécutives au manque d'oxygène) surviennent dès les premières minutes."
    },
    {
        question: "Qu'est ce qu'un arrêt cardiaque ?",
        reponse: "Le coeur ne fonctionne plus, ou d'une façon anarchique."
    },
    {
        question: "Quels sont les signes d'un arrêt cardiaque ?",
        reponse: "- Pas de réponse<br/>- Pas de réaction <br/>- Pas de respiration, ou respiration anormale"
    },
    {
        question: "Quel comportement doit-on adopter en présence d'une victime en arrêt cardiaque ?",
        reponse: "- <strong>ALERTER</strong> les secours<br/>- <strong>MASSER</strong> en faisant une réanimation cardio-pulmonaire<br/>- <strong>DEFIBRILLER</strong> avec un défibrillateur automatique si possible"
    },
    {
        question: "Qu'est ce qu'un défibrillateur automatisé externe (DAE) ?",
        reponse: "Appareil qui peut permettre de rétablir une activité cardiaque normale à une victime en arrêt cardiaque."
    },
    {
        question: "Quel est le risque principal d'un arrêt cardiaque sans intervention des secours ?",
        reponse: "La mort de la victime qui survient en quelques minutes"
    },
    {
        question: "L'utilisation d'un défibrillateur automatisé externe (DAE) sur une victime qui n'est pas en arrêt cardiaque présente-t-elle un risque ?",
        reponse: "Non, car le défibrillateur est automatisé et se déclenche uniquement quand la victime est en arrêt cardiaque."
    },
    {
        question: "Qu'est ce qu'une hémorragie ?",
        reponse: "Une perte de sang prolongée qui ne s'arrête pas. Elle imbibe un mouchoir en quelques secondes."
    },
    {
        question: "A partir de quel âge peut-on suivre une formation en premiers secours ?",
        reponse: "10 ans."
    },
    {
        question: "Quels sont les risques pour une personne victime d'une hémorragie ?",
        reponse: "- Une détresse circulatoire<br/>- Un arrêt cardiaque"
    },
    {
        question: "Comment arrêter une hémorragie ?",
        reponse: "En appuyant fortement sur l'endroit qui saigne avec les doigts ou la paume de la main, en mettant un tissu propre sur la plaie."
    }
];